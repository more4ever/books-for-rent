import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, ReactReduxContext } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import ReduxToastr from 'react-redux-toastr';
import { BrowserRouter } from 'react-router-dom';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import 'styles/index.scss';
import appConfig from 'constants/appConfig';
import store, { history } from 'store/store';
import App from 'App';

ReactDOM.render(
  <Provider store={store} context={ReactReduxContext}>
    <ConnectedRouter history={history} context={ReactReduxContext}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ConnectedRouter>
    <ReduxToastr
      progressBar
      closeOnToastrClick
    />
  </Provider>,
  document.getElementById('root'),
);

if (appConfig.isDev && module.hot) {
  module.hot.accept();
}
