/* eslint-disable global-require */
describe('appConfig testing', () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...OLD_ENV };
  });

  afterAll(() => {
    process.env = OLD_ENV;
  });

  it('isDev="true"', () => {
    process.env.NODE_ENV = 'development';

    const appConfig = require('./appConfig').default;

    expect(appConfig.isDev).toBe(true);
  });

  it('isDev="false"', () => {
    process.env.NODE_ENV = 'production';

    const appConfig = require('./appConfig').default;

    expect(appConfig.isDev).toBe(false);
  });

  it('apiUrl retrieved from environment', () => {
    const baseApiUrl = 'https://localhost:5000';
    process.env.REACT_APP_API_URL = baseApiUrl;

    const appConfig = require('./appConfig').default;

    expect(appConfig.apiUrl).toBe(baseApiUrl);
  });
});
