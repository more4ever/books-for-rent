export const routesByName = {
  home: '/',
  cart: '/cart',
};

export default {
  routesByName,
};
