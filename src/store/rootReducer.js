import { connectRouter } from 'connected-react-router';
import appReducer from 'modules/app/store/reducer';
import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';

import cartReducer from 'modules/cart/store/reducer';
import booksReducer from '../modules/books/store/reducer';

export default function createRootReducer(history) {
  return combineReducers({
    router: connectRouter(history),
    books: booksReducer,
    app: appReducer,
    cart: cartReducer,
    toastr: toastrReducer,
  });
}
