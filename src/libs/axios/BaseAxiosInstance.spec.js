import moxios from 'moxios';
import BaseAxiosInstance from 'libs/axios/BaseAxiosInstance';

describe('BaseAxiosInstance', () => {
  beforeEach(() => {
    moxios.install(BaseAxiosInstance);
  });

  afterEach(() => {
    moxios.uninstall(BaseAxiosInstance);
  });

  it('axios instance get base url from env variable', async () => {
    expect(BaseAxiosInstance.defaults.baseURL).toBe(process.env.REACT_APP_API_URL);
  });

  it('should destruct data fro success response', async (done) => {
    const onFulfilled = jest.fn();
    BaseAxiosInstance.get('/test').then(onFulfilled);

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      const responseBody = {
        id: 12345, firstName: 'Fred', lastName: 'Flintstone',
      };
      request.respondWith({
        status: 200,
        response: responseBody,
      })
        .then(() => {
          expect(onFulfilled.mock.calls.length).toBe(1);
          expect(onFulfilled.mock.calls[0][0]).toEqual(responseBody);
          done();
        });
    });
  });

  it('should throw an error with correct message for error response', async (done) => {
    const onRejected = jest.fn();
    BaseAxiosInstance.get('/test').catch(onRejected);

    const responseBody = {
      message: 'Test Error',
    };
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 400,
        response: responseBody,
      })
        .then(() => {
          const expectedError = onRejected.mock.calls[0][0];
          expect(expectedError).toBeInstanceOf(Error);
          expect(expectedError.message).toBe(responseBody.message);
          done();
        });
    });
  });
});
