import _merge from 'lodash.merge';
import thunk from 'redux-thunk';
// eslint-disable-next-line import/no-extraneous-dependencies
import configureStore from 'redux-mock-store';
import { booksInitialState } from 'modules/books/store/reducer';
import { appInitialState } from 'modules/app/store/reducer';
import { cartInitialState } from 'modules/cart/store/reducer';

const middlewares = [thunk];
const mockStoreLib = configureStore(middlewares);

export const getMockState = (state) => _merge(
  {
    app: appInitialState,
    cart: cartInitialState,
    books: booksInitialState,
  },
  state,
);

const mockStore = (state = {}) => mockStoreLib(getMockState(state));

export default mockStore;
