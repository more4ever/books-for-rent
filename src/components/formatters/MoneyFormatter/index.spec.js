import React from 'react';
import { render } from '@testing-library/react';
import MoneyFormatter from 'components/formatters/MoneyFormatter/index';

describe(
  'MoneyFormatter',
  () => {
    it('renders correctly with number value', () => {
      const { container } = render(<MoneyFormatter value={1} />);

      expect(container.textContent).toBe('$1.00');
    });

    it('renders correctly with string value', () => {
      const { container } = render(<MoneyFormatter value="2" />);

      expect(container.textContent).toBe('$2.00');
    });

    it('return "n/a" for invalid value', () => {
      const { container } = render(<MoneyFormatter value="asdasd" />);

      expect(container.textContent).toBe('n/a');
    });
  },
);
