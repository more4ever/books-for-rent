import * as PropTypes from 'prop-types';

const MoneyFormatter = (
  { value: valueInput },
) => {
  let value = valueInput;

  if (typeof value === 'string') {
    value = Number.parseFloat(value);
  }

  if (Number.isNaN(value)) {
    return 'n/a';
  }

  return (
    value.toLocaleString('en-US', { style: 'currency', currency: 'USD' })
  );
};

MoneyFormatter.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

MoneyFormatter.defaultProps = {};

export default MoneyFormatter;
