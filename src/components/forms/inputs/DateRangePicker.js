import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { DateRangePicker as DateRangePickerLib } from 'react-dates';
import moment from 'moment';

const DateRangePicker = (
  {
    onChange, startDate, endDate,
  },
) => {
  const [focusedInput, setFocusedInput] = useState(null);

  return (
    <DateRangePickerLib
      startDate={startDate}
      startDateId="start-date"
      endDate={endDate}
      endDateId="end-date"
      onDatesChange={onChange}
      focusedInput={focusedInput}
      onFocusChange={setFocusedInput}
    />
  );
};

DateRangePicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  startDate: PropTypes.instanceOf(moment),
  endDate: PropTypes.instanceOf(moment),
};

DateRangePicker.defaultProps = {
  startDate: null,
  endDate: null,
};

export default DateRangePicker;
