import cartSelectors from 'modules/cart/store/selectors';
import React, {} from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Container from 'reactstrap/es/Container';
import Navbar from 'reactstrap/es/Navbar';
import NavbarBrand from 'reactstrap/es/NavbarBrand';
import NavbarText from 'reactstrap/es/NavbarText';
import NavLink from 'reactstrap/lib/NavLink';
import { routesByName } from 'constants/routes';

const Header = (
  { cartTotal },
) => (
  <Navbar color="light" light>
    <Container>
      <NavbarBrand tag={Link} to={routesByName.home}>Books</NavbarBrand>
      <NavbarText>
        <NavLink tag={Link} to={routesByName.cart} className="">
          <u>
            Order Total:
            {' $'}
            {cartTotal}
          </u>
        </NavLink>
      </NavbarText>
    </Container>
  </Navbar>
);

Header.propTypes = {
  cartTotal: PropTypes.string.isRequired,
};

Header.defaultProps = {};

const mapStateToProps = (state) => ({
  cartTotal: cartSelectors.total(state),
});

export default connect(mapStateToProps)(Header);
