import React from 'react';
import { Link } from 'react-router-dom';
import { routesByName } from 'constants/routes';

const Page404 = () => (
  <div className="text-center p-4">
    404 Page not found
    <div className="pt-3">
      <Link to={routesByName.home}>
        Go Home
      </Link>
    </div>
  </div>
);

Page404.propTypes = {};

Page404.defaultProps = {};

export default Page404;
