import CartService from 'modules/cart/CartService';
import cartActionTypes from 'modules/cart/store/constants';
import cartSelectors from 'modules/cart/store/selectors';
import booksSelectors from 'modules/books/store/selectors';
import getReceiptPriceInfo from 'modules/cart/methods/getOrderPriceInfo';

const setCheckoutConfig = (config) => ({
  type: cartActionTypes.CART_SET_CHECKOUT_CONFIG,
  payload: config,
});

const setLoading = (value) => ({
  type: cartActionTypes.CART_SET_LOADING,
  payload: Boolean(value),
});

const setAddModalOpenState = (isOpen) => ({
  type: cartActionTypes.CART_SET_ADD_MODAL_OPEN_STATE,
  payload: Boolean(isOpen),
});

const setPriceInfo = (total) => ({
  type: cartActionTypes.CART_SET_PRICE_INFO,
  payload: total,
});

const syncPricesInfo = () => (dispatch, getState) => {
  const state = getState();

  const config = cartSelectors.checkoutConfig(state);
  const items = cartSelectors.items(state);
  const books = booksSelectors.getMap(state);

  dispatch(setPriceInfo(getReceiptPriceInfo(items, books, config)));
};

const syncCheckoutConfig = () => (dispatch) => {
  dispatch(setLoading(true));

  return CartService.getCheckoutConfig()
    .then(
      (response) => {
        dispatch(setCheckoutConfig(response));
        dispatch(syncPricesInfo());
      },
    )
    .finally(() => {
      dispatch(setLoading(false));
    });
};

const setItemForAdd = (item) => (dispatch) => {
  dispatch(setAddModalOpenState(item));

  dispatch({
    type: cartActionTypes.CART_SET_ITEM_FOR_ADD,
    payload: item,
  });
};

const addItem = (item) => async (dispatch) => {
  dispatch({
    type: cartActionTypes.CART_ADD_ITEM,
    payload: item,
  });
  dispatch(setItemForAdd(undefined));

  dispatch(syncPricesInfo());
};

const cartActions = {
  syncCheckoutConfig,
  setItemForAdd,
  addItem,
  syncPricesInfo,
};

export default cartActions;
