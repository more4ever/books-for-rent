import cartActionTypes from './constants';

export const cartInitialState = {
  checkoutConfig: undefined,
  loading: false,
  itemForAdd: undefined,
  items: [],
  isAddModalOpen: false,
  priceInfo: {
    total: '0',
    itemsInfo: {},
  },
};

const cartReducer = (state = { ...cartInitialState }, { type, payload }) => {
  switch (type) {
    case cartActionTypes.CART_SET_CHECKOUT_CONFIG:
      return { ...state, checkoutConfig: payload };
    case cartActionTypes.CART_SET_LOADING:
      return { ...state, loading: Boolean(payload) };
    case cartActionTypes.CART_SET_ITEM_FOR_ADD:
      return { ...state, itemForAdd: payload || cartInitialState.itemForAdd };
    case cartActionTypes.CART_ADD_ITEM:
      return { ...state, items: [...state.items, payload] };
    case cartActionTypes.CART_SET_ADD_MODAL_OPEN_STATE:
      return { ...state, isAddModalOpen: payload };
    case cartActionTypes.CART_SET_PRICE_INFO:
      return { ...state, priceInfo: payload || cartInitialState.priceInfo };
    default:
      return state;
  }
};

export default cartReducer;
