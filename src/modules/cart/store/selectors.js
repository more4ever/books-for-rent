const items = (state) => state.cart.items;
const itemForAdd = (state) => state.cart.itemForAdd;
const isAddModalOpen = (state) => state.cart.isAddModalOpen;
const checkoutConfig = (state) => state.cart.checkoutConfig;
const total = (state) => state.cart.priceInfo.total;
const pricesInfo = (state) => state.cart.priceInfo;

const cartSelectors = {
  items,
  itemForAdd,
  isAddModalOpen,
  checkoutConfig,
  total,
  pricesInfo,
};

export default cartSelectors;
