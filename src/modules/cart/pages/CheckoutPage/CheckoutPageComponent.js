import React from 'react';
import * as PropTypes from 'prop-types';
import Table from 'reactstrap/lib/Table';

import bookShape from 'modules/books/shapes/bookShape';
import HistoryItem from 'modules/cart/pages/CheckoutPage/components/CartItem';
import MoneyFormatter from 'components/formatters/MoneyFormatter';
import cartItemShape from 'modules/cart/shapes/cartItemShape';

const CheckoutPageComponent = (
  { pricesInfo, items, books },
) => (
  <>
    <h2>
      Selected items
    </h2>
    { items.length
      ? items.map((cartyItem) => {
        const {
          id, bookId,
        } = cartyItem;
        const bookInfo = books[bookId];
        const orderInfo = pricesInfo.itemsInfo[id];

        return (
          <HistoryItem
            className="mb-2"
            key={`receipt-item-${id}`}
            order={orderInfo}
            book={bookInfo}
            item={cartyItem}
          />
        );
      })
      : (
        <div className="p-4 text-center">
          Your cart is empty
        </div>
      )}
    <h2>
      Summary
    </h2>
    <Table>
      <tbody>
        <tr>
          <td>
            Total
          </td>
          <th className="text-right">
            <MoneyFormatter value={pricesInfo.total} />
          </th>
        </tr>
      </tbody>
    </Table>
  </>
);

CheckoutPageComponent.propTypes = {
  books: PropTypes.objectOf(bookShape).isRequired,
  items: PropTypes.arrayOf(cartItemShape).isRequired,
  pricesInfo: PropTypes.shape({
    total: PropTypes.string.isRequired,
    itemsInfo: PropTypes.objectOf(PropTypes.shape({
      length: PropTypes.number.isRequired,
      price: PropTypes.string.isRequired,
    })),
  }).isRequired,
};

CheckoutPageComponent.defaultProps = {};

export default CheckoutPageComponent;
