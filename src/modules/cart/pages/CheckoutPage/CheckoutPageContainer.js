import bookShape from 'modules/books/shapes/bookShape';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';

import booksSelectors from 'modules/books/store/selectors';
import cartSelectors from 'modules/cart/store/selectors';
import cartActions from 'modules/cart/store/actions';
import CheckoutPageComponent from 'modules/cart/pages/CheckoutPage/CheckoutPageComponent';
import cartItemShape from 'modules/cart/shapes/cartItemShape';

const CheckoutPageContainer = (
  {
    books, items, pricesInfo, syncPrices,
  },
) => {
  useEffect(
    () => {
      syncPrices();
    },
    [],
  );

  return (
    <CheckoutPageComponent
      books={books}
      items={items}
      pricesInfo={pricesInfo}
    />
  );
};

CheckoutPageContainer.propTypes = {
  books: PropTypes.objectOf(bookShape).isRequired,
  items: PropTypes.arrayOf(cartItemShape).isRequired,
  syncPrices: PropTypes.func.isRequired,
  pricesInfo: PropTypes.shape({}).isRequired,
};

const mapStateToProps = (state) => ({
  items: cartSelectors.items(state),
  books: booksSelectors.getMap(state),
  pricesInfo: cartSelectors.pricesInfo(state),
});

const mapDispatchToProps = {
  syncPrices: cartActions.syncPricesInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPageContainer);
