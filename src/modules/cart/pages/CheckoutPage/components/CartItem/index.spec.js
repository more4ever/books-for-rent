import React from 'react';
import moment from 'moment';
import { render } from '@testing-library/react';
import CartItem from 'modules/cart/pages/CheckoutPage/components/CartItem';

const endDate = moment('2020-09-08');

const props = {
  order: { length: 1, price: '1' },
  item: {
    id: '1', bookId: 1, startDate: endDate.subtract(1, 'day'), endDate,
  },
  book: { id: 1, name: 'test', type: 'novels' },
};

describe('CartItem tests', () => {
  it('should match snapshot', () => {
    const { container } = render(
      // eslint-disable-next-line react/jsx-props-no-spreading
      <CartItem {...props} />,
    );

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should assign class to root element', () => {
    const { container } = render(
      // eslint-disable-next-line react/jsx-props-no-spreading
      <CartItem {...props} className="test-class" />,
    );
    const domItem = container.firstChild;

    expect(domItem.classList.contains('test-class')).toBe(true);
  });

  it('should gracefully render error card if any param missing', () => {
    const { container } = render(
      <CartItem />,
    );

    expect(container.firstChild).toMatchSnapshot();
  });
});
