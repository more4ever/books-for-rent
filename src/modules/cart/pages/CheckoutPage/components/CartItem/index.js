import React, {} from 'react';
import * as PropTypes from 'prop-types';
import Card from 'reactstrap/lib/Card';
import CardBody from 'reactstrap/lib/CardBody';
import CardSubtitle from 'reactstrap/lib/CardSubtitle';
import CardTitle from 'reactstrap/lib/CardTitle';
import MoneyFormatter from 'components/formatters/MoneyFormatter';
import bookShape from 'modules/books/shapes/bookShape';
import Row from 'reactstrap/lib/Row';
import Col from 'reactstrap/lib/Col';
import cartItemShape from 'modules/cart/shapes/cartItemShape';

const CartItem = (
  {
    book,
    item,
    order,
    className,
  },
) => {
  if (!book || !item || !order) {
    return (
      <Card className={`${className}`}>
        <CardBody className="p-2">
          Unfortunately this ite can nott be processed
        </CardBody>
      </Card>
    );
  }

  const { startDate, endDate } = item;

  return (
    <Card className={`${className}`}>
      <CardBody className="p-2">
        <Row className="align-items-center">
          <Col>
            <CardTitle>
              <div>
                {book.name}
              </div>
              <div>
                {book.type}
              </div>
            </CardTitle>
            <CardSubtitle className="mb-2">
              {startDate.format('Do MMM, YYYY')}
              {' - '}
              {endDate.format('Do MMM, YYYY')}
              {` (${order.length} day(s))`}
            </CardSubtitle>

          </Col>
          <Col xs="auto">
            <MoneyFormatter value={order.price} />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

CartItem.propTypes = {
  className: PropTypes.string,
  book: bookShape.isRequired,
  order: PropTypes.shape({
    length: PropTypes.number.isRequired,
    price: PropTypes.string.isRequired,
  }).isRequired,
  item: cartItemShape.isRequired,
};

CartItem.defaultProps = {
  className: '',
};

export default CartItem;
