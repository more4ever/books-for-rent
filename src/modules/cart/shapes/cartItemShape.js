import * as PropTypes from 'prop-types';

const cartItemShape = PropTypes.shape({
  startDate: PropTypes.shape({}).isRequired,
  endDate: PropTypes.shape({}).isRequired,
  id: PropTypes.string.isRequired,
  bookId: PropTypes.number.isRequired,
});

export default cartItemShape;
