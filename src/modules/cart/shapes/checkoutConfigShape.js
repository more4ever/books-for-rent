import * as PropTypes from 'prop-types';

const checkoutConfigShape = PropTypes.shape({
  prices: PropTypes.shape({
    specialRules: PropTypes.arrayOf(
      PropTypes.shape({
        compare: PropTypes.oneOf(['less', 'above']).isRequired,
        length: PropTypes.number.isRequired,
        price: PropTypes.number.isRequired,
        duration: PropTypes.number,
      }),
    ),
  }),
});

export default checkoutConfigShape;
