import BaseAxiosInstance from '../../libs/axios/BaseAxiosInstance';

const CartService = Object.freeze({
  getCheckoutConfig() {
    return BaseAxiosInstance.get('/mocks/config/checkout.json');
  },
});

export default CartService;
