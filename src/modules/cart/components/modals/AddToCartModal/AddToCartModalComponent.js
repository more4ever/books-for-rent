import React from 'react';
import Button from 'reactstrap/es/Button';
import Col from 'reactstrap/es/Col';
import Modal from 'reactstrap/es/Modal';
import * as PropTypes from 'prop-types';
import DateRangePicker from 'components/forms/inputs/DateRangePicker';
import bookShape from 'modules/books/shapes/bookShape';
import ModalBody from 'reactstrap/es/ModalBody';
import ModalFooter from 'reactstrap/es/ModalFooter';
import ModalHeader from 'reactstrap/es/ModalHeader';
import Row from 'reactstrap/es/Row';

const AddToCartModalComponent = ({
  isOpen, toggle,
  onSubmit, onDatesChanged, submitDisabled,
  startDate, endDate, item,
}) => (
  <Modal isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>
      Add book to cart
    </ModalHeader>
    <ModalBody>
      {item && (
      <div className="pb-4">
        <Row>
          <Col>
            <img src={item.cover} alt={`${item.name} cover`} />
          </Col>
          <Col>
            {item.name}
          </Col>
        </Row>
      </div>
      )}
      <DateRangePicker
        startDate={startDate}
        endDate={endDate}
        onChange={onDatesChanged}
      />
    </ModalBody>
    <ModalFooter>
      <Row className="justify-content-end">
        <Col>
          <Button color="danger" onClick={toggle}>
            Cancel
          </Button>
        </Col>
        <Col>
          <Button color="success" onClick={onSubmit} disabled={submitDisabled}>
            Submit
          </Button>
        </Col>
      </Row>
    </ModalFooter>
  </Modal>
);

AddToCartModalComponent.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  submitDisabled: PropTypes.bool.isRequired,
  onDatesChanged: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  startDate: PropTypes.shape({}),
  endDate: PropTypes.shape({}),
  item: bookShape,
};

AddToCartModalComponent.defaultProps = {
  startDate: null,
  endDate: null,
  item: undefined,
};

export default AddToCartModalComponent;
