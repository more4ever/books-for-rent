import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';
import bookShape from 'modules/books/shapes/bookShape';
import AddToCartModalComponent from 'modules/cart/components/modals/AddToCartModal/AddToCartModalComponent';
import cartActions from 'modules/cart/store/actions';
import cartSelectors from 'modules/cart/store/selectors';

const initialState = {
  startDate: null,
  endDate: null,
};

class AddToCartModalContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = { ...initialState };
  }

  onClose = () => {
    const {
      toggle,
    } = this.props;
    toggle(false);
    this.setState({ ...initialState });
  }

  onDateRangeChange = (newDates) => {
    this.setState(newDates);
  }

  onSubmit = () => {
    const {
      onSubmit, item,
    } = this.props;
    const { startDate, endDate } = this.state;

    onSubmit({
      id: `${item.id}-${Date.now()}`, startDate, endDate, bookId: item.id,
    })
      .then(() => {
        this.setState({ ...initialState });
      });
  }

  isOpen() {
    const { isOpen, item } = this.props;

    return Boolean(isOpen && item);
  }

  isSubmitDisabled() {
    const { startDate, endDate } = this.state;

    return !startDate || !endDate;
  }

  render() {
    const {
      item,
    } = this.props;
    const { startDate, endDate } = this.state;

    return (

      <AddToCartModalComponent
        isOpen={this.isOpen()}
        toggle={this.onClose}
        item={item}
        submitDisabled={this.isSubmitDisabled()}
        onSubmit={this.onSubmit}
        startDate={startDate}
        endDate={endDate}
        onDatesChanged={this.onDateRangeChange}
      />
    );
  }
}
AddToCartModalContainer.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  item: bookShape,
};

AddToCartModalContainer.defaultProps = {
  item: undefined,
};

export default connect(
  (state) => ({
    isOpen: cartSelectors.isAddModalOpen(state),
    item: cartSelectors.itemForAdd(state),
  }),
  {
    onSubmit: cartActions.addItem,
    toggle: cartActions.setItemForAdd,
  },
)(AddToCartModalContainer);
