import moment from 'moment';

import getReceiptPriceInfo from 'modules/cart/methods/getOrderPriceInfo/index';

const incorrectCartItems = [
  {
    id: '1',
    bookId: 1,
    startDate: '2020-10-07T00:00:00.000Z',
    endDate: '2020-10-09T00:00:00.000Z',
  },
  {
    id: '2',
    bookId: 2,
    startDate: '2020-10-01T00:00:00.000Z',
    endDate: '2020-10-03T00:00:00.000Z',
  },
];

const correctCartItems = [
  {
    id: '1',
    bookId: 1,
    startDate: moment('2020-10-07T00:00:00.000Z'),
    endDate: moment('2020-10-09T00:00:00.000Z'),
  },
  {
    id: '2',
    bookId: 2,
    startDate: moment('2020-10-01T00:00:00.000Z'),
    endDate: moment('2020-10-03T00:00:00.000Z'),
  },
];

const books = {
  1: {
    id: 1,
    name: 'Book 1',
    type: 'regular',
  },
  2: {
    id: 1,
    name: 'Book 1',
    type: 'novels',
  },
};

const config = {
  prices: {
    regular: {
      price: 2,
    },
    novels: {
      price: 3,
    },
  },
};

describe(
  'calculateSingleItemPrice tests',
  () => {
    it('should correctly calculate total', () => {
      const result = getReceiptPriceInfo(correctCartItems, books, config);

      expect(result.total).toBe('10');
      expect(Object.keys(result.itemsInfo).length).toBe(2);
      expect(result.itemsInfo).toHaveProperty('1');
      expect(result.itemsInfo).toHaveProperty('2');
    });

    it('should return empty result for invalid config', () => {
      const result = getReceiptPriceInfo(correctCartItems, books, undefined);

      expect(result.total).toBe('0');
      expect(result.itemsInfo).toEqual({});
    });

    it('should return empty result for untransformed items', () => {
      const result = getReceiptPriceInfo(incorrectCartItems, books, config);

      expect(result.total).toBe('0');
      expect(result.itemsInfo).toEqual({});
    });

    it('should return empty result for orders without prices in config', () => {
      const result = getReceiptPriceInfo(correctCartItems, books, { prices: {} });

      expect(result.total).toBe('0');
      expect(result.itemsInfo).toEqual({});
    });
  },
);
