import BigNumber from 'bignumber.js';
import moment from 'moment';
import calculateOrderPrice from 'modules/cart/methods/calculateSingleItemPrice';

const getOrderPriceInfo = (orderItems, books, config) => {
  if (!orderItems || !config || !config.prices) {
    return {
      itemsInfo: {},
      total: '0',
    };
  }
  let total = new BigNumber(0);

  const itemsInfo = orderItems.reduce(
    (accum, {
      id, bookId, startDate, endDate,
    }) => {
      const book = books[bookId];

      if (!book || !moment.isMoment(startDate) || !moment.isMoment(endDate)) {
        return accum;
      }

      const bookConfig = config.prices[book.type];

      if (!bookConfig) {
        return accum;
      }

      const orderLength = endDate.diff(startDate, 'day');
      const itemPrice = calculateOrderPrice(bookConfig, orderLength);

      total = total.plus(itemPrice);

      // eslint-disable-next-line no-param-reassign
      accum[id] = {
        price: itemPrice.toString(10),
        length: orderLength,
      };

      return accum;
    },
    {},
  );

  return {
    itemsInfo,
    total: total.toString(10),
  };
};

export default getOrderPriceInfo;
