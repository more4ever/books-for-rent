import calculateOrderPrice from 'modules/cart/methods/calculateSingleItemPrice/index';

describe('calculateOrderPrice tests', () => {
  it('should return zero BigNumber', () => {
    const result = calculateOrderPrice();

    expect(result.toString(10)).toBe('0');
  });

  it('should correctly handle config without special rules', () => {
    const result = calculateOrderPrice({ price: 1 }, 2);

    expect(result.toString(10)).toBe('2');
  });

  it('should correctly skip specialRules if incorrect type', () => {
    const result = calculateOrderPrice({ price: 1, specialRules: null }, 2);

    expect(result.toString(10)).toBe('2');
  });

  it('should correctly handle special rule with "less" compare', () => {
    const result = calculateOrderPrice(
      {
        price: 1,
        specialRules: [
          {
            compare: 'less',
            length: 2,
            price: 10,
          },
        ],
      },
      1,
    );

    expect(result.toString(10)).toBe('10');
  });

  it('should correctly skip special rule with "less" compare', () => {
    const result = calculateOrderPrice(
      {
        price: 1,
        specialRules: [
          {
            compare: 'less',
            length: 2,
            price: 10,
          },
        ],
      },
      3,
    );

    expect(result.toString(10)).toBe('3');
  });

  it('should correctly handle special rule with "above" compare', () => {
    const result = calculateOrderPrice(
      {
        price: 1,
        specialRules: [
          {
            compare: 'above',
            length: 4,
            price: 1,
          },
        ],
      },
      5,
    );

    expect(result.toString(10)).toBe('5');
  });

  it('should correctly handle special rule with "above" compare and duration', () => {
    const result = calculateOrderPrice(
      {
        price: 4,
        specialRules: [
          {
            compare: 'above',
            length: 2,
            price: 1,
            duration: 2,
          },
        ],
      },
      5,
    );

    expect(result.toString(10)).toBe('14');
  });

  it('should correctly skip config with "above" compare', () => {
    const result = calculateOrderPrice(
      {
        price: 2,
        specialRules: [
          {
            compare: 'above',
            length: 4,
            price: 1,
          },
        ],
      },
      3,
    );

    expect(result.toString(10)).toBe('6');
  });
});
