import BigNumber from 'bignumber.js';

/**
 *
 * @param {object} config
 * @param {number} length
 * @return {BigNumber}
 */

const fallback = new BigNumber(0);

const calculateSingleItemPrice = (config, orderLength) => {
  if (!config || !config.price || !orderLength) {
    return fallback;
  }
  const regularPriceBn = new BigNumber(config.price);

  if (Array.isArray(config.specialRules) && config.specialRules.length) {
    for (let i = 0; i < config.specialRules.length; i += 1) {
      const priceRule = config.specialRules[i];
      const {
        compare, length, price, duration,
      } = priceRule;

      if (compare === 'less' && orderLength < length) {
        const priceBn = new BigNumber(price);
        return priceBn.multipliedBy(orderLength);
      }

      if (compare === 'above' && orderLength > length) {
        const priceBn = new BigNumber(price);
        if (duration) {
          const specialRuleLength = duration;
          const regular = orderLength - specialRuleLength;

          return priceBn.multipliedBy(specialRuleLength).plus(
            regularPriceBn.multipliedBy(regular),
          );
        }

        return priceBn.multipliedBy(orderLength);
      }
    }
  }

  return regularPriceBn.multipliedBy(orderLength);
};

export default calculateSingleItemPrice;
