import appActionTypes from 'modules/app/store/cosntants';

export const appInitialState = {
  isReady: false,
};

const appReducer = (state = { ...appInitialState }, { type }) => {
  switch (type) {
    case appActionTypes.APP_READY:
      return { ...state, isReady: true };
    default:
      return state;
  }
};

export default appReducer;
