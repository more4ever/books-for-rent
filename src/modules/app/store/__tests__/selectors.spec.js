import { getMockState } from 'tests/mocks/redux/mockStore';
import appSelectors from 'modules/app/store/selectors';

describe('appSelectors test', () => {
  const state = getMockState({ app: { isReady: true } });

  it('isReady selector should return correct value', () => {
    expect(appSelectors.isReady(state)).toBe(true);
  });
});
