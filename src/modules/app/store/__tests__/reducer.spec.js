import appReducer, { appInitialState } from 'modules/app/store/reducer';
import appActionTypes from 'modules/app/store/cosntants';

describe('App reducer tests', () => {
  let state;

  beforeEach(() => {
    state = { ...appInitialState };
  });

  it('should return correct initialState', () => {
    expect(appReducer(undefined, {})).toEqual(state);
  });

  it('should set isReady flag correctly', () => {
    state.isReady = true;
    expect(appReducer(undefined, { type: appActionTypes.APP_READY })).toEqual(state);
  });

  it('should set isReady flag correctly and ignore payload', () => {
    state.isReady = true;
    expect(appReducer(
      undefined,
      { type: appActionTypes.APP_READY, payload: false },
    )).toEqual(state);
  });
});
