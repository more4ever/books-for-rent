import moxios from 'moxios';
import appActions from 'modules/app/store/actions';
import BaseAxiosInstance from 'libs/axios/BaseAxiosInstance';
import appActionTypes from 'modules/app/store/cosntants';
import mockStore from 'tests/mocks/redux/mockStore';
import cartActionTypes from 'modules/cart/store/constants';

describe('App actions', () => {
  beforeEach(() => {
    moxios.install(BaseAxiosInstance);
  });

  afterEach(() => {
    moxios.uninstall(BaseAxiosInstance);
  });

  it('app should make initialization requests and logic', () => {
    const store = mockStore();

    moxios.stubRequest('/mocks/config/checkout.json', {
      status: 200,
      response: {
        price: 1,
      },
    });

    const expectedActions = [
      { type: cartActionTypes.CART_SET_LOADING, payload: true },
      { type: cartActionTypes.CART_SET_CHECKOUT_CONFIG, payload: { price: 1 } },
      {
        type: cartActionTypes.CART_SET_PRICE_INFO,
        payload: {
          itemsInfo: {},
          total: '0',
        },
      },
      { type: cartActionTypes.CART_SET_LOADING, payload: false },
      { type: appActionTypes.APP_READY },
    ];

    return store.dispatch(appActions.initialize()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('app should make initialization requests and logic', () => {
    const store = mockStore();

    moxios.stubRequest('/mocks/config/checkout.json', {
      status: 500,
      response: {
        message: 'Internal server error',
      },
    });

    const expectedActions = [
      { type: cartActionTypes.CART_SET_LOADING, payload: true },
      { type: cartActionTypes.CART_SET_LOADING, payload: false },
    ];

    return store.dispatch(appActions.initialize()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
