const APP_READY = 'APP_READY';

const appActionTypes = {
  APP_READY,
};

export default appActionTypes;
