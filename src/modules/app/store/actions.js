import appActionTypes from 'modules/app/store/cosntants';
import { toastr } from 'react-redux-toastr';
import cartActions from 'modules/cart/store/actions';

const appReady = () => ({
  type: appActionTypes.APP_READY,
});

const initialize = () => (dispatch) => Promise
  .all([
    dispatch(cartActions.syncCheckoutConfig()),
  ])
  .then(
    () => {
      dispatch(appReady());
    },
  ).catch((error) => {
    toastr.error('App initialization error', error.message);
  });

const appActions = {
  initialize,
};

export default appActions;
