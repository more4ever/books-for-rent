const isReady = (state) => state.app.isReady;

const appSelectors = {
  isReady,
};

export default appSelectors;
