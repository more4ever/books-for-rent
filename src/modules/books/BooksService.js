import BaseAxiosInstance from '../../libs/axios/BaseAxiosInstance';

const BooksService = Object.freeze({
  getList() {
    return BaseAxiosInstance.get('/mocks/book.json');
  },
});

export default BooksService;
