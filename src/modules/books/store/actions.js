import BooksService from '../BooksService';
import booksActionTypes from './constants';

const addItems = (list) => ({
  type: booksActionTypes.BOOKS_ADD_ITEMS,
  payload: list,
});

const setLoading = (value) => ({
  type: booksActionTypes.BOOKS_SET_LOADING,
  payload: value,
});

const getBooks = () => async (dispatch) => {
  dispatch(setLoading(true));

  return BooksService.getList()
    .then(
      /**
       * @param {array} response
       */
      (response) => {
        dispatch(addItems(
          response.reduce(
            (idToObjectAccum, book) => {
              // eslint-disable-next-line no-param-reassign
              idToObjectAccum[book.id] = book;
              return idToObjectAccum;
            },
            {},
          ),
        ));
      },
    )
    .finally(
      () => {
        dispatch(setLoading(false));
      },
    );
};

const booksActions = Object.freeze({
  getBooks,
});

export default booksActions;
