import booksActionTypes from './constants';

export const booksInitialState = {
  map: {},
  loading: false,
};

const booksReducer = (state = { ...booksInitialState }, { type, payload }) => {
  switch (type) {
    case booksActionTypes.BOOKS_ADD_ITEMS:
      return { ...state, map: { ...state.map, ...payload } };
    case booksActionTypes.BOOKS_SET_LOADING:
      return { ...state, loading: Boolean(payload) };
    default:
      return state;
  }
};

export default booksReducer;
