const BOOKS_ADD_ITEMS = 'BOOKS_ADD_ITEMS';
const BOOKS_SET_LOADING = 'BOOKS_SET_LOADING';

const booksActionTypes = Object.freeze({
  BOOKS_ADD_ITEMS,
  BOOKS_SET_LOADING,
});

export default booksActionTypes;
