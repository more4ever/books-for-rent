const getMap = (state) => state.books.map;
const loading = (state) => state.books.loading;

const booksSelectors = {
  getMap,
  loading,
};

export default booksSelectors;
