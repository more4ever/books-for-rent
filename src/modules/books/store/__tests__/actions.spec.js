import moxios from 'moxios';
import BaseAxiosInstance from 'libs/axios/BaseAxiosInstance';
import mockStore from 'tests/mocks/redux/mockStore';
import booksActionTypes from 'modules/books/store/constants';
import booksActions from 'modules/books/store/actions';

describe('Books actions test', () => {
  beforeEach(() => {
    moxios.install(BaseAxiosInstance);
  });

  afterEach(() => {
    moxios.uninstall(BaseAxiosInstance);
  });

  it('app should make initialization requests and logic', () => {
    const store = mockStore();
    const bookInResponse = {
      id: 1,
      name: 'test book',
    };

    moxios.stubRequest('/mocks/book.json', {
      status: 200,
      response: [bookInResponse],
    });

    const expectedActions = [
      { type: booksActionTypes.BOOKS_SET_LOADING, payload: true },
      {
        type: booksActionTypes.BOOKS_ADD_ITEMS,
        payload: {
          [bookInResponse.id]: bookInResponse,
        },
      },
      { type: booksActionTypes.BOOKS_SET_LOADING, payload: false },
    ];

    return store.dispatch(booksActions.getBooks()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('app should make initialization requests and logic', () => {
    const store = mockStore();
    const errorMessage = 'Internal server error';
    moxios.stubRequest('/mocks/book.json', {
      status: 500,
      response: {
        message: errorMessage,
      },
    });

    const expectedActions = [
      { type: booksActionTypes.BOOKS_SET_LOADING, payload: true },
      { type: booksActionTypes.BOOKS_SET_LOADING, payload: false },
    ];

    return store.dispatch(booksActions.getBooks())
      .catch((error) => {
        expect(store.getActions()).toEqual(expectedActions);
        expect(error).toBeInstanceOf(Error);
        expect(error.message).toBe(errorMessage);
      });
  });
});
