import booksReducer, { booksInitialState } from 'modules/books/store/reducer';
import booksActionTypes from 'modules/books/store/constants';

describe('Books reducer tests', () => {
  let state;

  beforeEach(() => {
    state = { ...booksInitialState };
  });

  it('should return correct initialState', () => {
    expect(booksReducer(undefined, {})).toEqual(state);
  });

  it('should set loading flag correctly to "true"', () => {
    state.loading = true;
    expect(booksReducer(
      undefined,
      { type: booksActionTypes.BOOKS_SET_LOADING, payload: true },
    )).toEqual(state);
  });

  it('should set loading flag correctly to "false"', () => {
    state.loading = true;
    const changedState = booksReducer(
      state,
      { type: booksActionTypes.BOOKS_SET_LOADING, payload: false },
    );

    expect(changedState.loading).toBe(false);
  });

  it('should correctly update existing books', () => {
    const existingBooks = {
      2: {
        id: 2,
        name: 'test',
      },
    };

    state.map = existingBooks;
    const updateState = booksReducer(
      state,
      {
        type: booksActionTypes.BOOKS_ADD_ITEMS,
        payload: {
          1: {
            id: 1,
            name: 'new book',
          },
          2: {
            id: 2,
            name: 'updated test',
          },
        },
      },
    );

    expect(Object.is(updateState.map, state.map)).toBe(false);
    expect(Object.is(updateState.map[2], existingBooks[2])).toBe(false);
    expect(updateState.map[2].name).toBe('updated test');
    expect(updateState.map[1]).not.toBeUndefined();
  });
});
