import { getMockState } from 'tests/mocks/redux/mockStore';
import booksSelectors from 'modules/books/store/selectors';

const store = {
  books: {
    map: {
      1: {
        id: 1,
        name: 'test',
      },
    },
    loading: true,
  },
};

describe('booksSelectors test', () => {
  const state = getMockState(store);

  it('getMap selector should return correct value', () => {
    expect(booksSelectors.getMap(state)).toEqual(store.books.map);
  });

  it('loading selector should return correct value', () => {
    expect(booksSelectors.loading(state)).toBe(true);
  });
});
