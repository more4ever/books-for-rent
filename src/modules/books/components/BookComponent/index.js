import React, { useCallback } from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from 'reactstrap/es/Button';
import Card from 'reactstrap/es/Card';
import CardBody from 'reactstrap/es/CardBody';
import CardFooter from 'reactstrap/es/CardFooter';
import CardImg from 'reactstrap/es/CardImg';

import bookShape from 'modules/books/shapes/bookShape';
import cartActions from 'modules/cart/store/actions';
import cssClasses from './styles.module.scss';

const BookComponent = (
  { book, addToCart: addToCartProp },
) => {
  const {
    cover, name, id, type,
  } = book;
  const addToCart = useCallback(
    () => {
      addToCartProp(book);
    },
    [addToCartProp, book],
  );

  return (
    <Card className={cssClasses.bookContainer}>
      <CardImg src={cover} alt={`${name} cover`} className={`card-img-top ${cssClasses.bookCover}`} />
      <CardBody className="card-body">
        <h6 className="card-title">
          {name}
          {' '}
          {id}
        </h6>
        <div>
          {type}
        </div>
      </CardBody>
      <CardFooter>
        <Button color="primary" onClick={addToCart}>
          Add to cart
        </Button>
      </CardFooter>
    </Card>
  );
};

BookComponent.propTypes = {
  book: bookShape.isRequired,
  addToCart: PropTypes.func.isRequired,
};

BookComponent.defaultProps = {};

const mapStateToProps = undefined;
const mapDispatchToProps = {
  addToCart: cartActions.setItemForAdd,
};

export default connect(mapStateToProps, mapDispatchToProps)(BookComponent);
