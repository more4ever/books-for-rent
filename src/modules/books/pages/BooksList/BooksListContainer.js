import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';
import { toastr } from 'react-redux-toastr';
import booksSelectors from 'modules/books/store/selectors';
import booksActions from 'modules/books/store/actions';
import { connect } from 'react-redux';
import bookShape from 'modules/books/shapes/bookShape';
import BooksListComponent from 'modules/books/pages/BooksList/BooksListComponent';

const BooksListContainer = (
  { list, loading, getList },
) => {
  useEffect(
    () => {
      getList()
        .catch((error) => {
          toastr.error(error.message);
        });
    },
    [],
  );

  return (
    <BooksListComponent loading={loading} list={list} />
  );
};

BooksListContainer.propTypes = {
  list: PropTypes.objectOf(bookShape).isRequired,
  loading: PropTypes.bool.isRequired,
  getList: PropTypes.func.isRequired,
};

BooksListContainer.defaultProps = {};

const masStateToProps = (state) => ({
  list: booksSelectors.getMap(state),
  loading: booksSelectors.loading(state),
});

const mapDispatchToProps = {
  getList: booksActions.getBooks,
};

export default connect(masStateToProps, mapDispatchToProps)(BooksListContainer);
