import React, {} from 'react';
import * as PropTypes from 'prop-types';
import BookComponent from 'modules/books/components/BookComponent';
import bookShape from 'modules/books/shapes/bookShape';

const BooksListComponent = (
  { list, loading },
) => {
  if (loading) {
    return (
      <div className="p-4 text-center">
        Loading...
      </div>
    );
  }
  /// Should be done in other way but time edges
  const arrayOfBooks = Object.values(list);

  if (!arrayOfBooks.length) {
    return (
      <div className="p-4 text-center">
        Sorry, any books for rent right now
      </div>
    );
  }

  return (
    <div className="row justify-content-center py-4">
      {arrayOfBooks.map(
        (book) => (
          <div className="col-auto" key={`single-book-${book.id}`}>
            <BookComponent book={book} />
          </div>
        ),
      )}
    </div>
  );
};

BooksListComponent.propTypes = {
  list: PropTypes.objectOf(bookShape).isRequired,
  loading: PropTypes.bool.isRequired,
};

BooksListComponent.defaultProps = {};

export default BooksListComponent;
