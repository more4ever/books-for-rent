import moment from 'moment';

/**
 *
 * @param {array} historyItems
 * @return {array}
 */
const receiptHistoryTransformer = (historyItems) => historyItems
  .map(
    ({
      startDate,
      endDate,
      ...rest
    }) => ({
      ...rest,
      startDate: moment(startDate),
      endDate: moment(endDate),
    }),
  );

export default receiptHistoryTransformer;
