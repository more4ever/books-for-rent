import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import { routesByName } from 'constants/routes';
import Page404 from 'staticPages/Page404';
import appActions from 'modules/app/store/actions';
import appSelectors from 'modules/app/store/selectors';
import Container from 'reactstrap/lib/Container';
import Header from 'components/layout/Header';
import BooksListContainer from 'modules/books/pages/BooksList/BooksListContainer';
import AddToCartModalContainer from 'modules/cart/components/modals/AddToCartModal/AddToCartModalContainer';
import CheckoutPageContainer from 'modules/cart/pages/CheckoutPage/CheckoutPageContainer';

class App extends PureComponent {
  componentDidMount() {
    const { initializeApp } = this.props;

    initializeApp();
  }

  render() {
    const { isReady } = this.props;

    if (!isReady) {
      return (
        <div className="min-vh-100 flex justify-content-center align-items-center">
          Please wait...
        </div>
      );
    }

    return (
      <>
        <Header />
        <Container className="py-4">
          <Switch>
            <Route exact path={routesByName.home} component={BooksListContainer} />
            <Route exact path={routesByName.cart} component={CheckoutPageContainer} />
            <Route exact path="*" component={Page404} />
          </Switch>
        </Container>
        <AddToCartModalContainer />
      </>
    );
  }
}

App.propTypes = {
  isReady: PropTypes.bool.isRequired,
  initializeApp: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isReady: appSelectors.isReady(state),
});

const mapDispatchToProps = {
  initializeApp: appActions.initialize,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
